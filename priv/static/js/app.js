$(function() {
    $('.file-share-btn').click(function() {
        var link = $(this).data('link');
        $("#shared-link-input").val(link);
        $('#file-share-modal').modal('show');
    });

    $(document).ajaxSend(function(e, xhr) {
        xhr.setRequestHeader('x-csrf-token',  $('#_csrf_token').val() );
    });

    $('#add-category-btn').click(function() {
        bootbox.prompt("Enter category name", function(result) {
            $.post('/category', {name: result})
            .then(function(category) {
                $('#category-list').append(
                    '<a href="/home/category/' + category.id + '"' 
                    + 'class="d-flex justify-content-between align-items-center list-group-item list-group-item-action">' 
                    + '<span class="text-fixed-container">' + category.name +'</span></a>'
                );
            });
        });
    });

    $('.file-delete-btn').click(function() {
        var parent = $(this).closest('.filelist-item');
        var id = parent.data('id');
        bootbox.confirm("Do you really want to delete this file?", function(result) {
            if(!result) {
                return;
            }
            
            $.ajax({
                type: "DELETE",
                url: "/file/" + id
            }).then(function() {
                parent.remove()
            });
        });
    });

});