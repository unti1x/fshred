defmodule FileShredder.Model.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user" do
    field :email, :string
    field :password, :string
    field :username, :string

    timestamps()
  end

  @spec hash_password(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp hash_password(%Ecto.Changeset{
    valid?: true,
    changes: %{password: password}
  } = changeset) do
    IO.inspect(changeset)
    change(changeset, password: Bcrypt.hash_pwd_salt(password))
  end

  defp hash_password(changeset), do: changeset

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password])
    |> validate_required([:username, :email, :password])
    |> hash_password()
  end

end
