defmodule FileShredderWeb.CategoryController do
  use FileShredderWeb, :controller

  alias FileShredder.CategoryManager

  def new(conn, %{"name" => name}) do
    user = Guardian.Plug.current_resource(conn)
    {:ok, category} = CategoryManager.create_category(
      %{name: name, user_id: user.id}
    )
    json(conn, %{id: category.id, name: category.name})
  end

end
