defmodule FileShredder.Repo do
  use Ecto.Repo,
    otp_app: :file_shredder,
    adapter: Ecto.Adapters.MySQL
end
