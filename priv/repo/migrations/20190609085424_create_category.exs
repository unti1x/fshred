defmodule FileShredder.Repo.Migrations.CreateCategory do
  use Ecto.Migration

  def change do
    create table(:category) do
      add :name, :string
      add :user_id, references(:user, on_delete: :nothing)

      timestamps()
    end

    create index(:category, [:user_id])
  end
end
