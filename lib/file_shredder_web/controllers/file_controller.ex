defmodule FileShredderWeb.FileController do
  use FileShredderWeb, :controller

  alias FileShredder.{CategoryManager, FileManager}

  @upload_dir "/tmp"

  def upload(conn, %{
    "category_id" => category_id,
    "file" => %Plug.Upload{
      path: tmp_path,
      # content_type: content_type,
      filename: filename
    } = file
  }) do
    user = Guardian.Plug.current_resource(conn)
    category = if category_id,
      do: CategoryManager.get_user_category(user.id, category_id),
      else: nil

    hash = tmp_path
      |> File.read!
      |> :erlang.crc32
      |> Integer.to_string(16)

    path = "#{hash}_#{filename}"

    {:ok, %File.Stat{size: size}} = File.stat(tmp_path)
    :ok = File.cp(tmp_path, "#{@upload_dir}/#{path}")
    {:ok, file} = FileManager.create_file(%{
      filename: filename,
      path: path,
      category_id: category.id,
      user_id: user.id
    })

    redirect(conn, to: "/home")
  end

  def show(conn, %{"id" => id}) do
    # user = Guardian.Plug.current_resource(conn)
    file = FileManager.get_file!(id)
    path = "#{@upload_dir}/#{file.path}"
    # if Canada.can?(user, :view, file) do
      send_download conn, {:file, path}, filename: file.filename
    # end
  end

  def delete(conn, %{"id" => id}) do
    file = FileManager.get_file!(id)
    path = "#{@upload_dir}/#{file.path}"
    File.rm!(path)
    {:ok, _} = FileManager.delete_file(file)
    json(conn, %{success: true})
  end

end
