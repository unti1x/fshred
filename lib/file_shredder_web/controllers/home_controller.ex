defmodule FileShredderWeb.HomeController do
  use FileShredderWeb, :controller

  alias FileShredder.{CategoryManager, FileManager}
  alias FileShredder.Model.User

  @spec current_user(Plug.Conn.t()) :: User.t() | nil
  def current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end

  @spec index(Plug.Conn.t(), any) :: Plug.Conn.t()
  def index(conn, params) do
    user = current_user(conn)
    categories = CategoryManager.get_user_categories(user)
    {category_id, _} = if params["id"],
      do: Integer.parse(params["id"]),
      else: {nil, nil}

    files = FileManager.get_user_files(user, category_id)
    all_files = FileManager.count_user_files(user)

    render conn, "index.html",
      show_add_btn: true,
      categories: categories,
      current_category: category_id,
      files: files,
      all_files: all_files
  end

end
