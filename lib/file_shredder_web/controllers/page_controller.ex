defmodule FileShredderWeb.PageController do
  use FileShredderWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
