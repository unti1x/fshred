defmodule FileShredder.Repo.Migrations.CreateFile do
  use Ecto.Migration

  def change do
    create table(:file) do
      add :filename, :string
      add :path, :string
      add :user_id, references(:user, on_delete: :nothing)
      add :category_id, references(:category, on_delete: :nothing)

      timestamps()
    end

    create index(:file, [:user_id])
    create index(:file, [:category_id])
  end
end
