defmodule FileShredderWeb.LayoutView do
  use FileShredderWeb, :view
  alias FileShredderWeb.Router.Helpers, as: Routes
  alias FileShredderWeb.Endpoint

  def auth_path(action) do
    Routes.auth_path(Endpoint, action)
  end

  def current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end

end
