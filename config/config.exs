# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :file_shredder,
  ecto_repos: [FileShredder.Repo]

# Configures the endpoint
config :file_shredder, FileShredderWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yFFybClpUgCOSJXOoIIrHyAURi34iv/jTZLRzWIw8zzXVpsyqJlr8rVj4Rv3TneB",
  render_errors: [view: FileShredderWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: FileShredder.PubSub, adapter: Phoenix.PubSub.PG2]

config :file_shredder, FileShredder.UserManager.Guardian,
  issuer: "file_shredder",
  secret_key: "SxsZ9jeLpnSnHy3CbMfGGqYY/okJymlcfPSzEinABQRv5DqMovVqw2ZHq7HZZL6K"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :canary, repo: FileShredder.Repo

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
