defmodule FileShredderWeb.HomeView do
  use FileShredderWeb, :view
  alias FileShredderWeb.Router.Helpers, as: Routes
  alias FileShredderWeb.Endpoint

  def category_path(nil) do
    Routes.home_path(Endpoint, :index)
  end

  def category_path(id) do
    Routes.home_path(Endpoint, :index, id)
  end

  def file_path(file \\ nil)
  def file_path(nil) do
    Routes.file_path(Endpoint, :upload)
  end

  def file_path(file) do
    Routes.file_path(Endpoint, :show, file.id, file.filename)
  end

  def share_link(file) do
    Routes.shared_url(Endpoint, :shared, file.path)
  end

end
