defmodule FileShredderWeb.AuthController do
  use FileShredderWeb, :controller

  alias FileShredder.{UserManager, Model.User, UserManager.Guardian}

  @spec login(Plug.Conn.t(), any) :: Plug.Conn.t()
  def login(conn, params) do
    render conn, "login.html", %{credentials: params["credentials"]}
  end

  @spec sign_in(Plug.Conn.t(), map) :: Plug.Conn.t()
  def sign_in(conn, %{
    "credentials" => %{"login" => login, "password" => password}
  }) do
    UserManager.authenticate_user(login, password)
    |> login_reply(conn, login)
  end

  @spec login_reply({atom, any}, Plug.Conn.t(), String.t()) :: Plug.Conn.t()
  defp login_reply({:ok, user}, conn, _username) do
    conn
    |> Guardian.Plug.sign_in(user)
    |> redirect(to: "/home") # TODO: use route generator
  end

  defp login_reply({:error, reason}, conn, username) do
    conn
    |> put_flash(:error, reason)
    |> login(%{"credentials" => %{"login" => username}})
  end

  @spec register(Plug.Conn.t(), any) :: Plug.Conn.t()
  def register(conn, params) do
    render conn, "register.html", %{
      user: Ecto.Changeset.cast(%User{}, params, [:username, :email])
    }
  end

  @spec new(Plug.Conn.t(), map) :: Plug.Conn.t()
  def new(conn, %{
    "password" => pwd,
    "password_confirmation" => confirm
  } = params) when pwd != confirm do
     conn
     |> put_flash(:error, "Password and confirmation don't match")
     |> register(params)
  end

  def new(conn, %{"user" => params}) do
    email_used = UserManager.email_taken?(params["email"])
    unless email_used do
      {:ok, user} = UserManager.create_user(params)

      conn
      |> Guardian.Plug.sign_in(user)
      |> redirect(to: "/home")
    else
      conn
      |> put_flash(:error, "The email is already used")
      |> register(params)
    end
  end

end
