defmodule FileShredderWeb.SharedController do
  use FileShredderWeb, :controller

  alias FileShredder.{FileManager}

  @upload_dir "/tmp"

  def shared(conn, %{"link" => link, "download" => "1"}) do
    file = FileManager.find_by_link(link)
    path = "#{@upload_dir}/#{file.path}"
    send_download conn, {:file, path}, filename: file.filename
  end

  def shared(conn, %{"link" => link}) do
    file = FileManager.find_by_link(link)
    render(conn, "share.html", %{file: file})
  end


end
