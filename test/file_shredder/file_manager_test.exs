defmodule FileShredder.FileManagerTest do
  use FileShredder.DataCase

  alias FileShredder.FileManager

  describe "files" do
    alias FileShredder.FileManager.File

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def file_fixture(attrs \\ %{}) do
      {:ok, file} =
        attrs
        |> Enum.into(@valid_attrs)
        |> FileManager.create_file()

      file
    end

    test "list_files/0 returns all files" do
      file = file_fixture()
      assert FileManager.list_files() == [file]
    end

    test "get_file!/1 returns the file with given id" do
      file = file_fixture()
      assert FileManager.get_file!(file.id) == file
    end

    test "create_file/1 with valid data creates a file" do
      assert {:ok, %File{} = file} = FileManager.create_file(@valid_attrs)
    end

    test "create_file/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = FileManager.create_file(@invalid_attrs)
    end

    test "update_file/2 with valid data updates the file" do
      file = file_fixture()
      assert {:ok, %File{} = file} = FileManager.update_file(file, @update_attrs)
    end

    test "update_file/2 with invalid data returns error changeset" do
      file = file_fixture()
      assert {:error, %Ecto.Changeset{}} = FileManager.update_file(file, @invalid_attrs)
      assert file == FileManager.get_file!(file.id)
    end

    test "delete_file/1 deletes the file" do
      file = file_fixture()
      assert {:ok, %File{}} = FileManager.delete_file(file)
      assert_raise Ecto.NoResultsError, fn -> FileManager.get_file!(file.id) end
    end

    test "change_file/1 returns a file changeset" do
      file = file_fixture()
      assert %Ecto.Changeset{} = FileManager.change_file(file)
    end
  end
end
