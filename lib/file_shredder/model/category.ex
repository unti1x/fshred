defmodule FileShredder.Model.Category do
  use Ecto.Schema
  import Ecto.Changeset

  schema "category" do
    field :name, :string

    belongs_to :user, FileShredder.Model.User

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :user_id])
    |> validate_required([:name, :user_id])
  end
end
