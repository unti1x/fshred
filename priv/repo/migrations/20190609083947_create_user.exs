defmodule FileShredder.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:user) do
      add :username, :string
      add :email, :string, size: 128
      add :password, :string

      timestamps()
    end

    create unique_index(:user, [:email])
  end
end
