# In this file, we load production configuration and
# secrets from environment variables. You can also
# hardcode secrets, although such is generally not
# recommended and you have to remember to add this
# file to your .gitignore.
use Mix.Config

database_url = "ecto://fileshredder:i\'m\ da\ shredder@127.0.0.1/fileshredder"

config :file_shredder, FileShredder.Repo,
  # ssl: true,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

secret_key_base = "U+y8BrIBkm4sZ3obxpgUOf4Fk3jvBVDWRzc9RMmJbtyJ0RvVhienG7g1IiiFvvMW"

#config :file_shredder, FileShredderWeb.Endpoint,
#  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],
#  secret_key_base: secret_key_base
