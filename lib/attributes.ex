alias FileShredder.Model.{User, File, Category}

defimpl Canada.Can, for: User do
  @spec can?(FileShredder.Model.User.t(), any, FileShredder.Model.File.t()) :: true
  def can?(%User{id: id}, _action, %File{user_id: user_id})
    when id === user_id, do: true

  def can?(%User{id: id}, _action, %Category{user_id: user_id})
    when id === user_id, do: true

  def can?(_user, _action, _model), do: false
end
