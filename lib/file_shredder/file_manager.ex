defmodule FileShredder.FileManager do
  @moduledoc """
  The FileManager context.
  """

  import Ecto.Query, warn: false
  alias FileShredder.Repo

  alias FileShredder.Model.{File, User}

  @doc """
  Returns the list of files.

  ## Examples

      iex> list_files()
      [%File{}, ...]

  """
  def list_files do
    Repo.all(File)
  end

  @doc """
  Gets a single file.

  Raises `Ecto.NoResultsError` if the File does not exist.

  ## Examples

      iex> get_file!(123)
      %File{}

      iex> get_file!(456)
      ** (Ecto.NoResultsError)

  """
  def get_file!(id), do: Repo.get!(File, id)

  @doc """
  Creates a file.

  ## Examples

      iex> create_file(%{field: value})
      {:ok, %File{}}

      iex> create_file(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_file(attrs \\ %{}) do
    %File{}
    |> File.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a file.

  ## Examples

      iex> update_file(file, %{field: new_value})
      {:ok, %File{}}

      iex> update_file(file, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_file(%File{} = file, attrs) do
    file
    |> File.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a File.

  ## Examples

      iex> delete_file(file)
      {:ok, %File{}}

      iex> delete_file(file)
      {:error, %Ecto.Changeset{}}

  """
  def delete_file(%File{} = file) do
    Repo.delete(file)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking file changes.

  ## Examples

      iex> change_file(file)
      %Ecto.Changeset{source: %File{}}

  """
  def change_file(%File{} = file) do
    File.changeset(file, %{})
  end

  defp user_files_query(%User{id: user_id}, nil) do
    from f in File, where: f.user_id == ^user_id
  end

  defp user_files_query(%User{id: user_id}, category_id) do
    from f in File, where: f.user_id == ^user_id and f.category_id == ^category_id
  end

  @spec get_user_files(User.t(), any) :: any
  def get_user_files(user, category_id) do
    Repo.all user_files_query(user, category_id)
  end

  @spec count_user_files(User.t()) :: integer
  def count_user_files(%User{id: user_id}) do
    query = from f in File,
      where: f.user_id == ^user_id,
      select: count(f.id)
    Repo.one(query)
  end

  def find_by_link(link) do
    query = from f in File, where: f.path == ^link
    Repo.one(query)
  end

end
