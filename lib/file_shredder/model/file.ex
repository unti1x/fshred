defmodule FileShredder.Model.File do
  use Ecto.Schema
  import Ecto.Changeset

  schema "file" do
    field :filename, :string
    field :path, :string

    belongs_to :user, FileShredder.Model.User
    belongs_to :category, FileShredder.Model.Category

    timestamps()
  end

  @doc false
  def changeset(file, attrs) do
    file
    |> cast(attrs, [:filename, :path, :user_id, :category_id])
    # |> cast_assoc(:user)
    # |> cast_assoc(:category)
    |> validate_required([:filename, :path, :user_id])
  end
end
