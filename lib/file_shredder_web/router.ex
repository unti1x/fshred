defmodule FileShredderWeb.Router do
  # import Canary.Plugs
  use FileShredderWeb, :router

  # alias FileShredder.Model.{Category, File}

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    # plug :load_and_authorize_resource, model: Category
    # plug :load_and_authorize_resource, model: File
  end

  pipeline :auth do
    plug FileShredder.UserManager.Pipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FileShredderWeb do
    pipe_through [:browser, :auth]

    get "/", PageController, :index

    get "/signin", AuthController, :login
    post "/signin", AuthController, :sign_in

    get "/signup", AuthController, :register
    post "/signup", AuthController, :new

  end

  scope "/", FileShredderWeb do
    pipe_through [:browser, :auth, :ensure_auth]

    get "/shared/:link", SharedController, :shared
    post "/category", CategoryController, :new
  end

  scope "/home", FileShredderWeb do
    pipe_through [:browser, :auth, :ensure_auth]
    get "/", HomeController, :index

    get "/category/:id", HomeController, :index
  end

  scope "/file", FileShredderWeb do
    pipe_through [:browser, :auth, :ensure_auth]
    post "/", FileController, :upload
    get "/:id/:filename", FileController, :show
    delete "/:id", FileController, :delete
  end

  # Other scopes may use custom stacks.
  # scope "/api", FileShredderWeb do
  #   pipe_through :api
  # end
end
