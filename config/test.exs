use Mix.Config

# Configure your database
config :file_shredder, FileShredder.Repo,
  username: "root",
  password: "",
  database: "file_shredder_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :file_shredder, FileShredderWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
